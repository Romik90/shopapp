<?php

declare(strict_types=1);

namespace App\Services\Notifier\Producer;

use App\Entity\Item;
use App\Services\Notifier\Event\ItemEvent;
use Redis;

final class Producer implements ProducerInterface
{
    /**
     * @var Redis
     */
    private $redisClient;

    public function __construct(Redis $redisClient)
    {
        $this->redisClient = $redisClient;
    }

    public function produce(Item $item): void
    {
        $this->redisClient->hSet(
            ItemEvent::BACK_TO_STOCK,
            $item->getId(),
            $item->getStatus()->getValue()
        );
    }
}
