<?php

declare(strict_types=1);

namespace App\Enumeration;

use App\Common\Enumerable;

final class ItemStatus extends Enumerable
{
    private const AVAILABLE = 'available';
    private const OUT_OF_STOCK = 'out of stock';

    protected function namesList(): array
    {
        return [
            self::AVAILABLE => 'Available',
            self::OUT_OF_STOCK => 'Out of stock',
        ];
    }

    public static function available(): self
    {
        return self::createEnum(self::AVAILABLE);
    }

    public static function outOfStock(): self
    {
        return self::createEnum(self::OUT_OF_STOCK);
    }
}
