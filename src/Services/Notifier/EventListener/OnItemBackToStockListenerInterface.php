<?php

namespace App\Services\Notifier\EventListener;

use App\Services\Notifier\Event\ItemEvent;

interface OnItemBackToStockListenerInterface
{
    public function handle(ItemEvent $event): void;
}
