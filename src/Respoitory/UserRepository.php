<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Item;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Iterator;

class UserRepository extends EntityRepository
{
    /**
     * @return User[]|Iterator
     */
    public function getUsersForItem(Item $item): Iterator
    {
        $queryBuilder = $this->createQueryBuilder('u')
            ->select('partial u.{id,locale,email}')
            ->where('i.id = :id')
            ->join(Item::class, 'i', Join::WITH, 'u.id = i.id')
            ->setParameter('id', $item->getId())
            ->orderBy('u.id')
        ;

        $users = $queryBuilder->getQuery()->iterate();

        foreach ($users as $user) {
            yield $user[0];

            $this->_em->clear();
        }
    }
}