<?php

declare(strict_types=1);

namespace App\Services\Notifier\Consumer;

interface ConsumerInterface
{
    public function consume(): void;
}
