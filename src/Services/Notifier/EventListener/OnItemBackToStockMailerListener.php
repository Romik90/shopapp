<?php

declare(strict_types=1);

namespace App\Services\Notifier\EventListener;

use App\Common\Message\MailerInterface;
use App\Common\Message\Message;
use App\Entity\Item;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Services\Notifier\Event\ItemEvent;
use Psr\Log\LoggerInterface;
use Symfony\Component\Translation\TranslatorInterface;

final class OnItemBackToStockMailerListener implements OnItemBackToStockListenerInterface
{
    /**
     * @var MailerInterface
     */
    private $mailer;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * @var string
     */
    private $fromEmail;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        MailerInterface $mailer,
        TranslatorInterface $translator,
        UserRepository $repository,
        string $fromEmail,
        LoggerInterface $logger
    ) {
        $this->mailer = $mailer;
        $this->translator = $translator;
        $this->repository = $repository;
        $this->fromEmail = $fromEmail;
        $this->logger = $logger;
    }

    public function handle(ItemEvent $event): void
    {
        /** @var Item */
        $item = $event->getItem();

        $users = $this->repository->getUsersForItem($item);

        try {
            /** @var $user User */
            foreach ($users as $user) {
                $body = $this->translator->trans(
                    'message.items.item_back_to_stock.body',
                    ['%title%' => $item->getTitle()],
                    null,
                    $user->getLocale()
                );

                $message = new Message();
                $message
                    ->setSubject($this->translator->trans(
                        'message.items.item_back_to_stock.title',
                        [],
                        null,
                        $user->getLocale()
                    ))
                    ->setFrom($this->fromEmail)
                    ->setTo($user->getEmail())
                    ->setBody($body)
                ;

                $this->mailer->send($message);
            }
        } catch (\Throwable $exception) {
            $this->logger->error('Item back to stock mailer notification failed', ['exception' => $exception]);
        }
    }
}
