<?php

declare(strict_types=1);

namespace App\Services\Notifier\Consumer;

use App\Repository\ItemRepository;
use App\Services\Notifier\Event\ItemEvent;
use Redis;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

final class Consumer implements ConsumerInterface
{
    /**
     * @var Redis
     */
    private $redisClient;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var RedirectDomainRepositoryInterface
     */
    private $repository;

    public function __construct(
        Redis $redisClient,
        ItemRepository $repository,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->redisClient = $redisClient;
        $this->repository = $repository;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function consume(): void
    {
        $items = $this->redisClient->hGetAll(ItemEvent::BACK_TO_STOCK);

        foreach ($items as $id => $status) {
            $item = $this->repository->findOneBy(['id' => $id]);

            $this->eventDispatcher->dispatch(
                ItemEvent::BACK_TO_STOCK,
                (new ItemEvent($item))
            );

            $this->redisClient->hDel(ItemEvent::BACK_TO_STOCK, $id);
        }
    }
}
