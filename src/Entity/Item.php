<?php

namespace App\Entity;

use App\Enumeration\ItemStatus;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ItemRepository")
 */
class Item
{
    /**
     * @var int|null
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank
     * @Assert\Length(min="5", max="255")
     */
    private $title;

    /**
     * @var ItemStatus
     *
     * @ORM\Column(type="item_status", options={"default": "available"})
     */
    private $status;

    /**
     * @var User[]|int
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="groups")
     */
    private $users;

    public function __construct()
    {
        $this->status = ItemStatus::available();
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getStatus(): ItemStatus
    {
        return $this->status;
    }

    public function setStatus(ItemStatus $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    /**
     * @param User[] $users
     */
    public function setUsers(Collection $users): self
    {
        $this->users = $users;

        return $this;
    }
}
