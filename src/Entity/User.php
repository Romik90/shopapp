<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Services\Notifier\Repository\Repository")
 */
class User
{
    /**
     * @var int|null
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank
     * @Assert\Email
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $locale;

    /**
     * @var Item[]
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Item", inversedBy="users")
     */
    private $favoriteItems;

    public function __construct()
    {
        $this->favoriteItems = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getLocale(): ?string
    {
        return $this->locale;
    }

    public function setLocale(?string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * @return Item[]
     */
    public function getFavoriteItems(): Collection
    {
        return $this->favoriteItems;
    }

    /**
     * @param Item[] $favoriteItems
     */
    public function setFavoriteItems(Collection $favoriteItems): self
    {
        $this->favoriteItems = $favoriteItems;

        return $this;
    }
}
