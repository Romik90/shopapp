<?php

declare(strict_types=1);

namespace App\Services\Notifier\Event;

use App\Entity\Item;
use Symfony\Component\EventDispatcher\Event;

final class ItemEvent extends Event
{
    public const BACK_TO_STOCK = 'item.back_to_stock';

    /**
     * @var Item
     */
    private $item;

    public function __construct(Item $item)
    {
        $this->item = $item;
    }

    public function getItem(): Item
    {
        return $this->item;
    }
}
