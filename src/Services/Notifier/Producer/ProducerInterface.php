<?php

declare(strict_types=1);

namespace App\Services\Notifier\Producer;

use App\Entity\Item;

interface ProducerInterface
{
    public function produce(Item $item): void;
}
