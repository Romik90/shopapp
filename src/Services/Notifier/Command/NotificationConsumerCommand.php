<?php

declare(strict_types=1);

namespace App\Services\Notifier\Command;

use App\Services\Notifier\Consumer\ConsumerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class NotificationConsumerCommand extends Command
{
    /**
     * @var ConsumerInterface
     */
    private $consumer;

    protected static $defaultName = 'app:redirect_domain:change_to_reserve:consumer';

    public function __construct(ConsumerInterface $consumer)
    {
        parent::__construct();

        $this->consumer = $consumer;
    }

    protected function configure(): void
    {
        $this->setDescription('Consumer for send notification about favorite item back to stock');
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $output->writeln('Process started!');
        $this->consumer->consume();
        $output->writeln('Process finished!');
    }
}
