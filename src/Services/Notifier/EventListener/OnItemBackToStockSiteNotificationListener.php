<?php

declare(strict_types=1);

namespace App\Services\Notifier\EventListener;

use App\Entity\Item;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Services\Notifier\Event\ItemEvent;
use DateTimeImmutable;
use App\Common\SiteNotification;
use App\Common\SiteNotificationService;
use Psr\Log\LoggerInterface;
use Symfony\Component\Translation\TranslatorInterface;

final class OnItemBackToStockSiteNotificationListener implements OnItemBackToStockListenerInterface
{
    /**
     * @var SiteNotificationService
     */
    private $notificationService;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        SiteNotificationService $notificationService,
        TranslatorInterface $translator,
        RepositoryInterface $repository,
        LoggerInterface $logger
    ) {
        $this->notificationService = $notificationService;
        $this->translator = $translator;
        $this->repository = $repository;
        $this->logger = $logger;
    }

    public function handle(ItemEvent $event): void
    {
        /** @var Item */
        $item = $event->getItem();

        $users = $this->repository->getUsersForItem($item);

        try {
            /** @var $user User */
            foreach ($users as $user) {
                $message = $this->translator->trans(
                    'message.items.item_back_to_stock.body',
                    ['%title%' => $item->getTitle()],
                    null,
                    $user->getLocale()
                );

                $notification = new SiteNotification();
                $notification
                    ->setUser($user)
                    ->setMessage($message)
                    ->setCreatedAt(new DateTimeImmutable())
                ;

                $this->notificationService->send($notification);
            }
        } catch (\Throwable $exception) {
            $this->logger->error('Item back to stock site notification failed', ['exception' => $exception]);
        }
    }
}
